package main

import (
	"flag"
	"fmt"
	"net/http"
	"net/url"

	glab "gitlab.com/kassio/gitlab-oauth-client-test/pkg/gitlab"
	"gitlab.com/kassio/gitlab-oauth-client-test/pkg/oauth"
	"gitlab.com/kassio/gitlab-oauth-client-test/pkg/router"
)

func main() {
	port := 8080
	flag.IntVar(&port, "port", port, "webserver port")
	clientID := ""
	flag.StringVar(&clientID, "client-id", clientID, "client id")
	clientSecret := ""
	flag.StringVar(&clientSecret, "client-secret", clientSecret, "client secret")
	gitlabURL := ""
	flag.StringVar(&gitlabURL, "gitlab-base-url", gitlabURL, "gitlab base url")
	flag.Parse()

	redirectURI := buildRedirectURI("/oauth/redirect", port)
	gitlab, err := glab.New(gitlabURL, clientID, clientSecret, redirectURI)
	if err != nil {
		fmt.Println("Failed to parse gitlab-base-url:", err)
		return
	}

	router := router.NewRouter()
	router.Handle("/", http.FileServer(http.Dir("public")))
	router.Handle("/login", oauth.Authorize(gitlab))
	router.Handle("/oauth/redirect", oauth.Redirect(gitlab))
	router.Start(port)
}

func buildRedirectURI(path string, port int) string {
	u := url.URL{
		Scheme: "http",
		Host:   fmt.Sprintf("localhost:%d", port),
		Path:   path,
	}

	return u.String()
}
