# Gitlab OAuth Client Test

Go client example for the [Gitlab OAuth Authorization Code Flow](https://docs.gitlab.com/ee/api/oauth2.html#authorization-code-flow)

```plantuml
@startuml
hide footbox

actor       "**User**"   as User   #lightyellow
participant "**App**"  as App  #violet
participant "**Gitlab**" as Gitlab #orangered

User   -[#f00]> App  ++#fbb: **GET** site

App    -[#f00]> User   --++#fbb: 302 gitlab.com/oauth/authorize?state=state1
note left
cookie state1
end note

User   -[#f00]> Gitlab ++#fbb: **GET** oauth/authorize?state=state1
Gitlab -[#f00]> User   --#fbb: 200 OK (auth form)
User   -[#f00]> Gitlab ++#fbb: **POST** auth form
Gitlab -[#f00]> User   --#fbb: 302 oauth/redirect

User   -[#f00]> App  ++#fbb: **GET** oauth/redirect?state=state1
App    -[#f00]> Gitlab ++#fbb: **POST** oauth/token?code=code1&state=state1
Gitlab -[#f00]> App  --#fbb: 200 OK (access_token)

App  -[#f00]> User   --#fbb: 302 site
User -[#f00]> App  ++#fbb: **GET** site
App  -[#f00]> User   --#fbb: OK 200
deactivate User

@enduml
```