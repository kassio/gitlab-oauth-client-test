run:
	go build -o bin/gitlab-oauth-client-test
	bin/gitlab-oauth-client-test \
		-client-id="$(GITLAB_OAUTH_TEST_CLIENT_ID)" \
		-client-secret="$(GITLAB_OAUTH_TEST_CLIENT_SECRET)" \
		-gitlab-base-url="http://gdk.test:3000"
