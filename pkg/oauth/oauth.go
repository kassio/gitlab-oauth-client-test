package oauth

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"net/http"

	glab "gitlab.com/kassio/gitlab-oauth-client-test/pkg/gitlab"
	"gitlab.com/kassio/gitlab-oauth-client-test/pkg/logger"
)

type Response struct {
	AccessToken  string `json:"access_token"`
	TokenType    string `json:"token_type"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	CreatedAt    int    `json:"created_at"`
	Scope        string `json:"scope"`
}

func Authorize(gitlab glab.Gitlab) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		state := generateState()
		gitlab = gitlab.Endpoint(
			"oauth/authorize",
			map[string]string{
				"state":         state,
				"response_type": "code",
				"scope":         "api"})

		http.SetCookie(w, &http.Cookie{
			Name:  "state",
			Value: state,
		})

		endpoint := gitlab.URL
		endpoint.RawQuery = gitlab.Query.Encode()

		logger.Log{
			"msg": "redirecting",
			"url": endpoint.String(),
		}.Print()

		http.Redirect(w, r, endpoint.String(), http.StatusTemporaryRedirect)
	})
}

func Redirect(gitlab glab.Gitlab) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		gitlab = gitlab.Endpoint(
			"oauth/token",
			map[string]string{
				"code":       r.URL.Query().Get("code"),
				"grant_type": "authorization_code"})

		cookieState, err := r.Cookie("state")
		if err != nil {
			logger.Log{"msg": "no previous state set!"}.Print()
		}

		if r.URL.Query().Get("state") != cookieState.Value {
			logger.Log{
				"msg": "\n\n>>>>>>>>> DIFFERENT STATE!!! <<<<<<<<<<<<<<<\n\n",
			}.Print()

			http.Redirect(w, r, "/unauthorized.html", http.StatusUnauthorized)
			return
		}

		logger.Log{
			"msg":  "PostForm",
			"url":  gitlab.String(),
			"form": gitlab.Query,
		}.Print()

		res, err := http.PostForm(gitlab.String(), gitlab.Query)
		if err != nil {
			fmt.Println("Failed to get token:", err)
		}
		defer res.Body.Close()

		var t Response
		if err := json.NewDecoder(res.Body).Decode(&t); err != nil {
			fmt.Println("could not parse JSON response:", err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		fmt.Print(
			"\n* AccessToken: ", t.AccessToken,
			"\n* RefreshToken: ", t.RefreshToken,
			"\n* TokenType: ", t.TokenType,
			"\n* Scope: ", t.Scope,
			"\n* ExpiresIn: ", t.ExpiresIn,
			"\n* CreatedAt: ", t.CreatedAt,
			"\n\n",
		)

		http.Redirect(w, r, "/welcome.html", http.StatusTemporaryRedirect)
	})
}

func generateState() string {
	bytes := make([]byte, 20)
	if _, err := rand.Read(bytes); err != nil {
		return ""
	}

	return hex.EncodeToString(bytes)
}
