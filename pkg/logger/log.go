package logger

import (
	"fmt"
	"time"
)

type Log map[string]any

func (l Log) Print() {
	msg := fmt.Sprintf("[%s]", time.Now().Format("2006/01/02 15:04:05"))

	for k, v := range l {
		if v == nil || v == "" {
			continue
		}

		msg = fmt.Sprintf("%s %s: %+v", msg, k, v)
	}

	fmt.Println(msg)
}
