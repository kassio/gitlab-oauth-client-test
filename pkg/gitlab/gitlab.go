package gitlab

import (
	"net/url"
)

type Gitlab struct {
	url.URL
	Query url.Values
}

func New(baseURL, clientID, clientSecret, redirectURI string) (Gitlab, error) {
	parsedURL, err := url.Parse(baseURL)
	if err != nil {
		return Gitlab{}, err
	}

	params := url.Values{
		"client_id":     []string{clientID},
		"client_secret": []string{clientSecret},
		"redirect_uri":  []string{redirectURI},
	}

	return Gitlab{
		URL:   *parsedURL,
		Query: params,
	}, nil
}

func (g Gitlab) Endpoint(path string, params map[string]string) Gitlab {
	g.Path = path

	for key, value := range params {
		g.Query.Set(key, value)
	}

	return g
}
