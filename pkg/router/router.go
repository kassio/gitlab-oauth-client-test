package router

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	"gitlab.com/kassio/gitlab-oauth-client-test/pkg/logger"
)

// middleware alias to avoid the long function description internally
type middleware = func(http.Handler) http.Handler

type Router struct {
	*http.ServeMux
	defaultMiddlewares []middleware
}

// NewRouter creates a new Server.
// The given middlewares are be executed in the given order.
func NewRouter(middlewares ...middleware) Router {
	return Router{
		ServeMux: http.NewServeMux(),
		defaultMiddlewares: append(
			[]middleware{logMiddleware},
			middlewares...,
		),
	}
}

// Handle registers a new handler for the given pattern.
// The optional middlewares are executed in the given order,
// wrapping the given handler.
func (r Router) Handle(route string, handler http.Handler, middlewares ...middleware) {
	ms := append(r.defaultMiddlewares, middlewares...)

	for i := len(ms) - 1; i >= 0; i-- {
		handler = ms[i](handler)
	}

	r.ServeMux.Handle(route, handler)
}

// Start the web server.
func (r Router) Start(port int) {
	fmt.Println("Starting server at:", port)

	log.Fatal(
		http.ListenAndServe(fmt.Sprintf(":%d", port), r.ServeMux))
}

// log the request information
func logMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		buf := new(strings.Builder)
		_, _ = io.Copy(buf, r.Body)

		logger.Log{
			"method": r.Method,
			"path":   r.URL.Path,
			"params": r.URL.Query().Encode(),
			"body":   buf.String(),
		}.Print()

		h.ServeHTTP(w, r)
	})
}
